# sisop-praktikum-modul-1-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |

# Penjelasan Soal Nomor 1

Code university_survey.sh :

```
#Menampilka 5 Universitas dengan ranking tertinggi di Jepang
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5

#Menampilkan Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5 | sort -r -t ',' -k 9

#Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 10 | sort -k 20

#Menampilkan universitas dengan kata kunci keren.
grep -i "keren" 2023\ QS\ World\ University\ Rankings.csv 

```
A) Menggunakan fungi "grep" untuk mengambil data yang kita inginkan yang memiliki nama "Japan" kemudian kita batasi 5 baris awalan dengan menggunakan fungsi head -n 5
```
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5
```

B) Fungsinya sama dengan bagian A namun terdapat fungsi sorting yang digunakan untuk mendata paling besar (Default besar ke kecil) pada kolom ke 9 untuk dapat menandai kolom ke 9 menggunakan fungsi "-k <angka kolom>" dan "-t" adalah sebagai pemisah kolom dan "-n" adalah fungsi numerik
```
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | head -n 5 | sort -n -t ',' -k 9 
```

C) Fungsinua sama dengan bagian B sedangkan kita sorting terlebih dahulu untuk mengurutkan nilai terbesar ke terkecil kemudian kita ambil 10 bagian saja (Revisi bagian Asdos), Kesalah saya waktu demo mengambil 5 baris pertama terlebih dahulu kemudian disorting
```
grep "Japan" 2023\ QS\ World\ University\ Rankings.csv | sort -n -t ',' -k 20 | head -n 10
```

D) Fungsi sama seperti grep "Japan" tetapi ditambahkan "-i <kata yang ingin dicari>" ignore case untuk mencocokkan kata tersebut dalam teks tanpa memperhatikan kata tersebut besar maupun kecil
```
grep -i "keren" 2023\ QS\ World\ University\ Rankings.csv 
```

# Penjelasan Soal Nomor 2

Code kobeni_liburan.sh 

```
#!/bin/bash
hour=$(date +"%-H")

#membuat interval menjadi jam
INTERVAL = $1

#menghitung jam sekarang
if [$hour -eq 0]; then
 count=1
else
 count=$hour
fi

#membuat folder kumpulan
j=1
mkdir kumpulan_$j

#looping mendownload gambar
while true;do
 for ((i=1; i<=$count; i++)); do
  echo >> kumpulan_$j/perjalanan_$i
done

#tidak jalan selama 10 jam
  sleep 36000

#looping kumpulan selanjutnya
  hour=$(date +"%-H")
  if ((hour % INTERVAL == 0))
  then
    j=$((j + 1))
    mkdir kumpulan_$j
  fi
done

#membuat zip dalam satu hari
TODAY=$(date +%d)
if [ ! -f "devil_${j}.zip" ] || [ "$TODAY" != "$(date -r devil_${j}.zip +%d)" ]
then
  zip -r "devil_${j}.zip" "kumpulan_$j"
fi

``` 

Pada bagian pertama mencari jam sekarang dengan cara "%H" untuk mendapatkan waktu sekarang sedangkan $date untuk mendapatkan hari tanggal  jam menit detik sekarang, jika jam 00 maka gambar akan mengeluarkan 1 jika tidak maka akan mengeluarkan jumlah waktu sekarang.

```
#!/bin/bash
#jalab script setiap 10 jam
#0 */10 * * * /home/vagrant/Downloads/Praktikum/soal2/kobeni_liburan_revisi.sh

#Jalan pada pukul 23 59
#59 23 * * * /home/vagrant/Downloads/Praktikum/soal2/kobeni_liburan_revisi.sh

# Hitung waktu
HOUR=$(date +"%-H")
if [ $HOUR -eq 0 ]; then
  NUM_IMAGES=1
else
  NUM_IMAGES=$HOUR
fi
```



Pada bagian folder pertama-tama kita membuat variable yang dimana pencarian alamat folder dari script yang kita jalankan pada bagian $dirname untuk mencari alamat ${BASH_SOURCE[0]} alamat folder script yang sedang dijalankan. Pada bagian loop folder untuk mencari apakah folder bagian pertama sudah ada atau belum jika sudah ada maka variabel angka ditambahkan dan membuat mkdir kumpulan_$n
```
# cari folder
fld=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
kumpulan='kumpulan_'
n=1

# buat folder
if [ $HOUR != 2359 ]; then
 while [ -d $fld/$kumpulan$n ]; do ((n++))
 done

 mkdir "$fld/$kumpulan$n"

 HOUR=$(date +"%-H")
 if [ $HOUR == 0 ]; then
  HOUR = 1
 fi
fi

```





Pada bagian gambar melakukan loop sebanyak waktu sekarang dengan mendownload gambar pada link tersebut, karena sebelumnya hanya mendownload gambar maka perlu dipindahkan ke dalam folder kumpulan dengan cara mv, maka gambar akan otomatis masuk kedalam folder terbaru.
```
# Download gambar
for ((i=1; i<=$NUM_IMAGES; i++)); do
   wget -O perjalanan_$i.jpg https://source.unsplash.com/random
done

# Pindah gambar kedalam folder
for ((i=1; i<=$NUM_IMAGES; i++)); do
  mv perjalanan_$i.jpg $fld/$kumpulan$n/
done
```

Pada bagian akhir diperlukan membuat zip untuk mengumpulkan semua folder menjadi 1, mencari zip terbaru terlebih dahulu dengan mengelist semua folder dalam alamat tersebut dengan ls kemudian grep untuk mencari nama zip setelah itu baru kita bisa membuat zip dengan nama terbaru dan memasukkan semua folder kumpulan kedalammnya dan mengahapus -rm folder yang berada diluar zip
```
# buat zip pada jam 12 malam
if [ $HOUR -eq 2359 ]; then
 devil='devil_'
 d=1
 
 while [ ' ls $fld | grep $devil$d.zip' ]; do ((d++))
 done

 cd $fld
 zip $devil$d  -rm 'find . -name "kumpulan_*" -print'
fi

```

# Penjelasan Soal Nomor 3

Code for louis.sh :
``` 
#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if password meets the criteria
if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || "$password" =~ $username || "$password" =~ chicken || "$password" =~ ernie ]]; then
    echo -e "\nPassword does not meet the criteria"
    exit 1
fi

# check if the username already exists in the users file
if grep -q "^$username:" ./users/users.txt; then
    echo "REGISTER: ERROR User already exists" >> ./users/log.txt
    echo "User already exists"
    exit 1
fi

# add the username and password to the users file
echo "$username:$password" >> ./users/users.txt
echo "REGISTER: INFO User $username registered successfully" >> ./users/log.txt
echo "User registered successfully"
```
File ini merupakan sistem register pada script, tentunya dengan beberapa ketentuan seperti
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil 
3. Alphanumeric 
4. Tidak boleh sama dengan username
5. Tidak boleh menggunakan kata chicken atau ernie   
Dengan beberapa ketentuan tersebut maka dibuat kode seperti 

` if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || "$password" =~ $username || "$password" =~ chicken || "$password" =~ ernie ]]; then
    echo -e "\nPassword does not meet the criteria"
    exit 1 
`
Data username serta password yang berhasil didaftarkan akan tercatat pada file users.txt dengan penambahan `./users/users.txt` setelah kode pengecekan kriteria 

Lalu pada setiap percobaan login yang berhasil maupun gagal karena tidak memenuhi kriteria, akan tercatat pada log.txt. Message pada log akan berbeda tergantung aksi yang dilakukan user

kode ` ./users/log.txt ` ditambahkan didepan message log pada kode untuk memasukan _message log_ pada file log.txt pada sistem register maupun login

Code for retep.sh :
```
#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if the username and password match
if ! grep -q "^$username:$password$" ./users/users.txt; then
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
    echo "Invalid username or password"
    exit 1
fi

echo "LOGIN: INFO User $username logged in" >> ./users/log.txt
echo "Welcome, $username"

```
Pada sistem login terdapat kode untuk pengecekan data yang terdaftar menggunakan _conditional statement_ dengan kode 

`if ! grep -q "^$username:$password$" ./users/users.txt; then
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
    echo "Invalid username or password"
    exit 1
fi`


Percobaan login yang berhasil maupun tidak berhasil tertulis pada bagian `./users/log.txt` sama seperti sistem register diatas.

# Penjelasan Soal Nomor 4
Untuk nomor 4, diperlukan 2 proses tahapan yaitu enkripsi dan deskripsi

### Enkripsi
mengambil data jam dan tanggal yg kemudian disimpan di variabel data
```
DATE=$(date +"%H:%M %d-%m-%Y")
data="$DATE.txt"
```
baca file log sistem yang berada di ``/test1/subtest2/subsubtest``
```
log="$(cat /test1/subtest2/subsubtest`)"
```
A-Z diulang 2 kali diakrenakan huruf z bisa jadi ditambah 23
```
alfabet=abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz
```
catat jam dilakukannya enkripsi, simpan di variabel hour, kemudian translate hasil enkripsi dengan command tr 

```
hour=$(date +"%H") 
encrypt=$(printf '\n%s' "$log" | tr "${alfabet:0:26}" "${alfabet:${jam}:26}")
```
#deskripsi


```
echo "Input nama file yang ingin di decrypt (hh:mm dd:mm:yyyy): "
07:06 03:03:2023 # input
ADD=$(awk 'NR==1{print}' "$nama.txt")
hour=$(date +"%H:%M %d:%m:%Y")
data="Hasil Decrypt $hour.txt"
```
awk seluruh file enkripsi
```
syslog="$(awk '{
  if((n == 0)){
    n++
  }
  else {
    print
    n++
  }
}' "$nama.txt")"
``` 
pada proses ini hanya perlu reverse apfabet
```
alfabet_reverse=zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba
decrypt=$(echo "$log" | tr "${alfabet_reverse:0:26}" "${alfabet_reverse:${ADD}:26}")

echo "$decrypt" > "$data"
```
