#!/bin/bash

read -p "Enter username: " username
read -p "Enter password: " -s password

# check if the username and password match
if ! grep -q "^$username:$password$" ./users/users.txt; then
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./users/log.txt
    echo "Invalid username or password"
    exit 1
fi

echo "LOGIN: INFO User $username logged in" >> ./users/log.txt
echo "Welcome, $username"
